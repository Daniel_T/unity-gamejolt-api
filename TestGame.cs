using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TestGame : MonoBehaviour {

	const int GAME_ID = 0; //Your Game ID
	const string GAME_KEY = " "; //Your Game Key
	string USER_NAME = "";
	string USER_TOKEN = "";
	
	GameJoltAPI gj;
	
	void Start () {
		
		//Initalise the GameJoltAPI instance
		GameJoltAPI.Instance.Init(GAME_ID, GAME_KEY);
		
		//Turn on Debug 
		GameJoltAPI.Instance.SetVerbose(true);
		
		// Get the user's credentials
		USER_NAME = ""; // Get this from a text field in your game.
		USER_TOKEN = ""; // Get this from a password field in your game.
		
		// Set our Callbacks, these functions will be called from GameJoltAPI.cs
		GameJoltAPI.Instance.verifyUserCallback = VerifyUserCallback;
		GameJoltAPI.Instance.highscoreCallback = HighscoresCallback;
		GameJoltAPI.Instance.getVerifyUserCallback = GetVerifiedUserCallback;
		GameJoltAPI.Instance.addHighscoreCallback = AddHighscoreCallback;
		GameJoltAPI.Instance.setDataStoreCallback = SetDataStoreCallback;
		GameJoltAPI.Instance.getDataStoreCallback = GetDataStoreCallback;
		GameJoltAPI.Instance.removeDataStoreCallback = RemoveDataStoreCallback;
		GameJoltAPI.Instance.getDataStoreKeysCallback = GetDataStoreKeysCallback;
		GameJoltAPI.Instance.sessionOpenCallback = SessionOpenCallback;
		GameJoltAPI.Instance.sessionUpdateCallback = SessionUpdateCallback;
		GameJoltAPI.Instance.sessionCloseCallback = SessionCloseCallback;
		GameJoltAPI.Instance.achieveTrophyCallback = AchieveTrophyCallback;
		GameJoltAPI.Instance.getTrophiesCallback = GetTrophiesCallback;
		GameJoltAPI.Instance.getTrophyCallback = GetTrophyCallback;
		GameJoltAPI.Instance.getRankCallback = GetRankCallback;
	}
	
	#region GUI
	
	void OnGUI () {
		
		if (GUI.Button (new Rect (10,10,150,50), "Get All Highscores")) {
       		GameJoltAPI.Instance.GetHighscores(true);
		}
		
		if (GUI.Button (new Rect (10,80,150,50), "Get User Highscores")) {
       		GameJoltAPI.Instance.GetHighscores(false);
		}
		
		if (GUI.Button (new Rect (10,150,150,50), "Verify User")) {
			if(USER_NAME == "" || USER_TOKEN == "") {
				Debug.Log ("USER_NAME or USER_TOKEN needs to be set in TestGame.cs");	
			}
			else {
       			GameJoltAPI.Instance.VerifyUser(USER_NAME, USER_TOKEN);
			}
		}
		
		if (GUI.Button (new Rect (10,220,150,50), "Get Verified User")) {
       		GameJoltAPI.Instance.GetVerifiedUser();
		}
		
		if (GUI.Button (new Rect (200,10,150,50), "Post Highscore (User)")) {
       		GameJoltAPI.Instance.AddHighscore("Over 10000!", 10001); 
		}
		
		if (GUI.Button (new Rect (200,80,150,50), "Post Highscore (Guest)")) {
       		GameJoltAPI.Instance.AddHighscore("Guest Name", "Under 10000!", 9999);
		}
		
		if (GUI.Button (new Rect (390,10,150,50), "Set Data Store (Game)")) {
       		GameJoltAPI.Instance.SetDataStore(DataStore.DataStoreType.GAME, "game_data", "Some piece of game data");
		}
		
		if (GUI.Button (new Rect (390,80,150,50), "Get Data Store (Game)")) {
       		GameJoltAPI.Instance.GetDataStore(DataStore.DataStoreType.GAME, "game_data");
		}
		
		if (GUI.Button (new Rect (390,150,150,50), "Remove Data Store (Game)")) {
       		GameJoltAPI.Instance.RemoveDataStore(DataStore.DataStoreType.GAME, "game_data");
		}
		
		if (GUI.Button (new Rect (390,220,150,50), "Get Data Keys (Game)")) {
       		GameJoltAPI.Instance.GetDataStoreKeys(DataStore.DataStoreType.GAME);
		}
		
		if (GUI.Button (new Rect (580,10,150,50), "Set Data Store (User)")) {
       		GameJoltAPI.Instance.SetDataStore(DataStore.DataStoreType.USER, "user_data", "Some piece of user data");
		}
		
		if (GUI.Button (new Rect (580,80,150,50), "Get Data Store (User)")) {
       		GameJoltAPI.Instance.GetDataStore(DataStore.DataStoreType.USER, "user_data");
		}
		
		if (GUI.Button (new Rect (580,150,150,50), "Remove Data Store (User)")) {
       		GameJoltAPI.Instance.RemoveDataStore(DataStore.DataStoreType.USER, "user_data");
		}
		
		if (GUI.Button (new Rect (580,220,150,50), "Get Data Keys (User)")) {
       		GameJoltAPI.Instance.GetDataStoreKeys(DataStore.DataStoreType.USER);
		}
		
		if (GUI.Button (new Rect (10,290,150,50), "Open Session")) {
       		GameJoltAPI.Instance.SessionOpen();
		}
		
		if (GUI.Button (new Rect (10,360,150,50), "Update Session")) {
       		GameJoltAPI.Instance.SessionUpdate(true);
		}
		
		if (GUI.Button (new Rect (10,430,150,50), "Close Session")) {
       		GameJoltAPI.Instance.SessionClose();
		}
		
		if (GUI.Button (new Rect (200,150,150,50), "Achieve Trophy")) {
       		GameJoltAPI.Instance.AchieveTrophy(58);
		}
		
		if (GUI.Button (new Rect (200,220,150,50), "Get Trophies")) {
       		GameJoltAPI.Instance.GetTrophies(Trophy.Achieved.EMPTY);
		}
		
		if (GUI.Button (new Rect (200,290,150,50), "Get Trophy")) {
       		GameJoltAPI.Instance.GetTrophy(58);
		}
		
		if (GUI.Button (new Rect (200,360,150,50), "Get Rank")) {
       		GameJoltAPI.Instance.GetRank(1000, "");
		}
	}
	
	#endregion
	
	#region Callbacks
	
	public void VerifyUserCallback (bool verified) {
		
		Debug.Log ("VerifyUserCallback : " + verified);
	}
	
	public void HighscoresCallback (List<Highscore> highscores) {
		
		Debug.Log ("HighscoresCallback :");
		
		if(highscores != null) {
			for (int i = 0; i < highscores.Count; i++) {
				Debug.Log(highscores[i]);
			}
		}	
	}
	
	public void GetVerifiedUserCallback (User user) {
		
		Debug.Log ("GetVerifiedUserCallback : " + user.getName());
	}
	
	public void AddHighscoreCallback (bool success) {
		
		Debug.Log ("AddHighscoreCallback : Success = " + success);
	}
	
	public void SetDataStoreCallback (DataStore dataStore) {
		
		Debug.Log ("SetDataStoreCallback : dataStore = " + dataStore);
	}
	
	public void GetDataStoreCallback (DataStore dataStore) {
		
		Debug.Log ("GetDataStoreCallback : dataStore = " + dataStore);
	}
	
	public void RemoveDataStoreCallback (bool success)  {
		
		Debug.Log ("RemoveDataStoreCallback : Success = " + success);
	}
	
	public void GetDataStoreKeysCallback (List<string> keys) {
		
		Debug.Log ("GetDataStoreKeysCallback : keys = " + keys);
	}
	
	public void SessionOpenCallback (bool success) {
		
		Debug.Log ("SessionOpenCallback : success = " + success);
	}
	
	public void SessionUpdateCallback (bool success) {
		
		Debug.Log ("SessionUpdateCallback : success = " + success);
	}
	
	public void SessionCloseCallback (bool success) {
		
		Debug.Log ("SessionCloseCallback : success = " + success);
	}
	
	public void AchieveTrophyCallback (bool success) {
		
		Debug.Log ("AchieveTrophyCallback : success = " + success);
	}
	
	public void GetTrophiesCallback (List<Trophy> trophies) {
		
		Debug.Log ("GetTrophiesCallback : Trophies = " + trophies);
	}
	
	public void GetTrophyCallback (Trophy trophy) {
		
		Debug.Log ("GetTrophyCallback : Trophy = " + trophy);
	}
	
	public void GetRankCallback (int rank) {
		
		Debug.Log ("GetRankCallback : Rank = " + rank);
	}
	
	#endregion
}
