using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

/**
 * <b>Trophy</b><br/>
 * A trophy is an achievement in the GameJolt API.
 * 
 * @author Ashley Gwinnell
 * @since 0.90
 * @version 0.90
 */
public class Trophy 
{
	/** The Difficulty level enumeration. */
	public enum Difficulty {BRONZE, SILVER, GOLD, PLATINUM};
	
	/** The Achieved almost-boolean type enumeration. */
	public enum Achieved {TRUE, FALSE, EMPTY};
	
	/** The Trophy properties */
	private Dictionary<string, string> properties;
	
	/**
	 * Create a new Trophy.
	 */
	public Trophy() {
		properties = new Dictionary<string, string>();
	}
	
	/**
	 * Adds a property to the Trophy.
	 * @param key The key by which the property can be accessed.
	 * @param value The value for the key.
	 */
	public void addProperty(string key, string value) {
		properties[key] = value;
	}
	
	/**
	 * Gets a property of the Trophy that isn't specified by a specific method.
	 * This exists for forward compatibility.
	 * @param key The key of the Trophy attribute you want to obtain.
	 * @return A property of the Trophy that isn't specified by a specific method.
	 */
	public string getProperty(string key) {
		return properties[key];
	}
	
	/**
	 * Get the ID of the Trophy.
	 * @return The ID of the Trophy.
	 */
	public string getId() {
		return this.getProperty("id");
	}
	/**
	 * Get the name of the Trophy.
	 * @return The name of the Trophy.
	 */
	public string getTitle() {
		return this.getProperty("title");
	}
	
	/**
	 * Get the description of the Trophy.
	 * @return The description of the Trophy.
	 */
	public string getDescription() {
		return this.getProperty("description");
	}
	
	/**
	 * Get the difficulty of the Trophy. 
	 * i.e. Bronze, Silver, Gold, Platinum.
	 * @return The difficulty of the Trophy.
	 */
	public Difficulty getDifficulty() {
		return (Difficulty) Enum.Parse(typeof(Difficulty), this.getProperty("difficulty")); //// Difficulty.valueOf();
	}
	
	/**
	 * Determines whether the Trophy is achieved or not.
	 * @return True if the verified user has the Trophy.
	 */
	public bool isAchieved() {
		return Boolean.Parse(this.getProperty("achieved"));
	}
	
	/**
	 * Gets the URL of the Trophy's image.
	 * @return The URL of the Trophy's image.
	 */
	public string getImageURL() {
		return this.getProperty("image_url");
	}
	
	public override string ToString() {
		StringBuilder b = new StringBuilder();
		b.Append("Trophy [id=");
		b.Append(this.getId());
		b.Append(", title=");
		b.Append(this.getTitle());
		b.Append("]");
		return b.ToString();
	}
}