using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

/**
 * <b>User</b><br/>
 * This class is created and populated using the GameJoltAPI class.
 * 
 * @author Ashley Gwinnell
 * @since 0.95
 * @version 0.97
 */
public class User {

	/** The different types of users that are available at Game Jolt. */
	public enum UserType {USER, DEVELOPER, MODERATOR, ADMIN};
	
	/** This enum type dictates whether the user is banned or not! */
	public enum UserStatus {ACTIVE, BANNED};
	
	/** The User properties map */
	private Dictionary<string, string> properties;

	
	
	public User() {
		properties = new Dictionary<string, string>();
	}
	
	/**
	 * Adds a property to the User.
	 * @param key The key by which the property can be accessed.
	 * @param value The value for the key.
	 */
	public void addProperty(string key, string value) {
		properties.Add(key, value);
	}
	
	/**
	 * Gets a property of the User that isn't specified by a specific method.
	 * This exists for forward compatibility.
	 * @param key The key of the User attribute you want to obtain.
	 * @return A property of the User that isn't specified by a specific method.
	 */
	public string getProperty(string key) {
		return properties[key];
	}
	
	public void setName(string s) {
		this.properties["username"] =  s;
	}
	public void setToken(string s) {
		this.properties["token"] = s;
	}
	public void setType(UserType t) {
		this.properties["type"] = t.ToString();
	}
	public void setStatus(UserStatus s) {
		this.properties["status"] = s.ToString();
	}
	
	public string getName() {
		return this.properties["username"];
	}
	public string getToken() {
		return this.properties["token"];
	}
	public UserType getType() {
		return (UserType) Enum.Parse(typeof(UserType), this.properties["type"]); 
	}
	public string getAvatarURL() {
		return this.properties["avatar_url"];
	}
	public UserStatus getStatus() {
		return (UserStatus) Enum.Parse(typeof(UserStatus), this.properties["status"]);
	}
	public string getDeveloperName() {
		return this.properties["developer_name"];
	}
	public string getDeveloperWebsite() {
		return this.properties["developer_website"];
	}
	public string getDeveloperDescription() {
		return this.properties["developer_description"];
	}
	
	public override string ToString() {
		StringBuilder b = new StringBuilder();
		b.Append("User [name=");
		b.Append(getName());
		b.Append(", token=");
		b.Append(getToken());
		b.Append(", type=");
		b.Append(getType());
		b.Append(", avatar_url=");
		b.Append(getAvatarURL());
		b.Append("]");
		return b.ToString();
	}
	
}