using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

/**
 * <b>GameJoltAPI</b><br/>
 * This is the main class that you use to implement the GameJolt Trophy, DataStore and HighScore systems.
 * 
 * @since 0.90
 * @version 1.0
 * @author Ashley Gwinnell + Daniel Twomey
 */
public class GameJoltAPI : MonoBehaviour {

	private const string protocol = "http://";
	private string api_root = "gamejolt.com/api/game/";
	
	private int gameId;
	private string privateKey;
	
	private int version;
	
	private string username;
	private string usertoken;
	
	private string m_latestResponse;

	private bool verbose;
	private bool verified;
	
	private static GameJoltAPI instance;
 
	public static GameJoltAPI Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new GameObject ("GameJoltAPI").AddComponent<GameJoltAPI> ();
				
				//Set some default values
				instance.api_root = "gamejolt.com/api/game/";
				instance.version = 1;
				instance.verified = false;
				instance.verbose = false;
			}
 
			return instance;
		}
	}
 
	public void OnApplicationQuit ()
	{
		instance = null;
	}

	/// <summary>
	/// Init the specified gameId and privateKey.
	/// </summary>
	/// <param name='gameId'>
	/// Game identifier.
	/// </param>
	/// <param name='privateKey'>
	/// Private key.
	/// </param>
	public void Init(int gameId, string privateKey) {
		this.gameId = gameId;
		this.privateKey = privateKey;
	}
	
	/// <summary>
	/// Initializes a new instance of the <see cref="GameJoltAPI"/> class.
	/// </summary>
	/// <param name='gameId'>
	/// Game identifier.
	/// </param>
	/// <param name='privateKey'>
	/// Private key.
	/// </param>
	/// <param name='username'>
	/// Username.
	/// </param>
	/// <param name='usertoken'>
	/// Usertoken.
	/// </param>
	public GameJoltAPI(int gameId, string privateKey, string username, string usertoken) {
		this.gameId = gameId;
		this.privateKey = privateKey;
		this.VerifyUser(username, usertoken);
	}
	
	/**
	 * Set the version of the GameJolt API to use.
	 * @param version The version of the GameJolt API to be using.
	 */
	public void SetVersion(int version) {
		this.version = version;
	}
	
	/**
	 * Get the version of the GameJolt API you are using. 
	 * @return The API version in use.
	 */
	public double GetVersion() {
		return version;
	}
	
	/**
	 * Sets whether the API should print out debug information to the Console.
	 * By default, this is set to true.
	 * @param b whether the API should print out debug informationto the Console.
	 */
	public void SetVerbose(bool b) {
		this.verbose = b;
	}
	
	/**
	 * Returns true if the GJ API is set to print out it's debug information.
	 * @return True if the GJ API is set to print out it's debug information.
	 */
	public bool GetIsVerbose() {
		return verbose;
	}
	
	/**
	 * Check whether the user/player has verified their credentials.
	 * @return whether the user/player has verified their credentials or not.
	 */
	public bool GetIsVerified() {
		return verified;
	}
	
	/**
	 * You'll probably not need to do this for games -- used to test the Game & Service API in development.
	 * i.e. not on gamejolt.com
	 */
	public void OverrideApiRoot(string root) {
		api_root = root;
	}
	
	private string IterableToString(IEnumerator<string> s) {
		return s.Current;
	}
	
	public delegate void VerifyUserCallback (bool verified);
	public VerifyUserCallback verifyUserCallback;
	
	/**
	 * Attempt to verify the Players Credentials.
	 * @param username The Player's Username.
	 * @param userToken The Player's User Token.
	 */
	public void VerifyUser(string username, string userToken)
	{
		Debug.Log (api_root);
		
		this.username = username;
		this.usertoken = userToken;
		
		this.verified = false;
		Dictionary<string, string> paramss = new Dictionary<string, string>();
		paramss["username"] = username;
		paramss["user_token"] = userToken;
    	
		this.Request("users/auth/", paramss, false, ReadVerifyUserResponse);
	}
	
	/// <summary>
	/// Reads the verify user response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadVerifyUserResponse (string response, params System.Object[] values) {
				
		if (verbose) { Debug.Log("GameJoltAPI:Verify User response recieved, Reading."); }
		
		bool verified = false;
		
		string[] lines = response.Split('\n');
		
		try {
			string line = lines[0];
    		
			string[] parts = line.Split(':');
			
			string t = parts[1].Substring(1, 4);
			//Debug.Log(t);
			if (t.Equals("true")) {
				this.verified = true;
				verified = true;
			}
			
		} catch (Exception e) {
			if (this.verbose) { 
				StringBuilder bu = new StringBuilder();
				bu.Append("GameJoltAPI: Could not verify user because the response from GJ was invalid.\n");
				bu.Append("Response was: ");
				bu.Append(response);
				bu.Append("\n");
				bu.Append("\n");
				bu.Append(e.Message);
				Debug.Log(bu.ToString());
				verified = false;
			}
		} 
		
		verifyUserCallback (verified);
	}
	
	public delegate void GetVerifyUserCallback (User verifiedUser);
	public GetVerifyUserCallback getVerifyUserCallback;
	
	/**
	 * Verify a User using the username and private key variables
	 * Calls GetVerifyUserCallback
	 */ 
	public void GetVerifiedUser() {
		if (this.verified) {
			
			Dictionary<string, string> paramsss = new Dictionary<string, string>();
			paramsss["username"] = ""+this.username + this.privateKey;
				 
			string url = this.GetRequestURL("users/", paramsss, false);
			paramsss["username"] = ""+this.username;
			paramsss["signature"] = this.MD5(url);
				
			url = this.GetRequestURL("users/", paramsss, false);
			StartCoroutine (OpenURLAndGetResponse(url, ReadGetVerifiedUserResponse, null));

		}  else {
			if (this.verbose) { Debug.Log("GameJoltAPI: Could not get the (currently verified) user."); }
		}
	}
	
	/// <summary>
	/// Reads the get verified user response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadGetVerifiedUserResponse (string response, params System.Object[] values) {
				
		string[] lines = response.Split('\n');
		if (!lines[0].Trim().Equals("success:\"true\"")) {
			if (verbose) { 
				Debug.Log("GameJoltAPI: Could not get the Verified User with Username: " + this.username, null); 
				Debug.Log(response);
			}
			return;
		}
				
		User u = new User();
		for (int i = 1; i < lines.Length; i++) {
			if (lines[i].Equals("")) { continue; }
			
			string key = lines[i].Substring(0, lines[i].IndexOf(':'));
			int vfi = lines[i].IndexOf(':')+2;
			int vli = lines[i].LastIndexOf('"');
			string v = lines[i].Substring(vfi, vli - vfi);
			if (key.Equals("type")) {
				u.setType((User.UserType) Enum.Parse(typeof(User.UserType), v.ToUpper()));
			} else if (key.Equals("status")) {
				u.setStatus((User.UserStatus) Enum.Parse(typeof(User.UserStatus), v.ToUpper()));
			} else {
				u.addProperty(key, v);
			}
		}
		u.setName(this.username);
		u.setToken(this.usertoken);

		getVerifyUserCallback (u);
	}
	
	public delegate void SetDataStoreCallback (DataStore dataStore);
	public SetDataStoreCallback setDataStoreCallback;
	
	/**
	 * Adds a piece of data to Game Jolt's servers replacing 
	 * any that already exists with the key and type given.
	 * 
	 * @param type The type of the Data Store. Should be either DataTypeStore.USER or DataTypeStore.GAME.
	 * @param key The key for which to store the data. You use this key to retrieve the DataStore.
	 * @param data Data to keep on Game Jolt servers. This data is normally xml, json, or serialised memory.
	 */
	public void SetDataStore(DataStore.DataStoreType type, string key, string data) {
		
		DataStore ds = new DataStore();
		ds.setKey(key);
		ds.setData(data);
		ds.setType(type);
		
		if (type == DataStore.DataStoreType.GAME) {
		
			Dictionary<string, string> paramss = new Dictionary<string, string>();
			paramss["data"] = ""+data;
			paramss["key"] = ""+key+this.privateKey;
			
			string url = this.GetRequestURL("data-store/set", paramss, false);
			paramss["key"] = key;
			paramss["signature"] = this.MD5(url);
			url = this.GetRequestURL("data-store/set", paramss, false);
			
			StartCoroutine (OpenURLAndGetResponse(url, ReadSetGameDataStoreResponse, ds));
		} else {
			this.Request("data-store/set", "key=" + key + "&data=" + data, ReadSetGameDataStoreResponse, ds);
		}
	}
	
	/// <summary>
	/// Reads the set game data store response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadSetGameDataStoreResponse (string response, params System.Object[] values) {
			
		if (verbose) { Debug.Log(response); }
		
		if (response.Contains("success:\"false\"")) {
			if (verbose) { Debug.Log("GameJoltAPI: Could not add " + (values[0] as DataStore).getType() + " DataStore with Key \"" + (values[0] as DataStore).getKey() + "\"."); }
			if (verbose) { Debug.Log(response); }
			setDataStoreCallback (null);
		}
		else if (response.Contains("success:\"true\"")) {
			setDataStoreCallback ((values[0] as DataStore));
		}
	}
	
	public delegate void GetDataStoreCallback (DataStore dataStore);
	public GetDataStoreCallback getDataStoreCallback;
	
	/**
	 * Retrieve a piece of data from Game Jolt's servers
	 * as specified by type and key.
	 * 
	 * @param type The type of the Data Store. Should be either DataTypeStore.USER or DataTypeStore.GAME.
	 * @param key The key for which the data was stored.
	 */
	public void GetDataStore(DataStore.DataStoreType type, string key) {
		
		DataStore ds = new DataStore();
		ds.setKey(key);
		ds.setType(type);
		
		if (type == DataStore.DataStoreType.GAME) {
			Dictionary<string, string> paramss = new Dictionary<string, string>();
			paramss["format"] = "dump";
			paramss["key"] = ""+key+this.privateKey;
				
			string url = this.GetRequestURL("data-store/", paramss, false);
			paramss["key"] = key;
			paramss["signature"] = this.MD5(url);
			url = this.GetRequestURL("data-store/", paramss, false);
				
			StartCoroutine (OpenURLAndGetResponse(url, ReadGetGameDataStoreResponse, ds));
			
		} else {
			this.Request("data-store/", "key=" + key + "&format=dump", ReadGetGameDataStoreResponse, ds);
		}
	}
	
	/// <summary>
	/// Reads the get game data store response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>/
	public void ReadGetGameDataStoreResponse (string response, params System.Object[] values) {
				
		if (!response.Substring(0, 7).Equals("SUCCESS")) {
			if (verbose) { Debug.Log("GameJoltAPI: " + response.Substring(9)); }
			if (verbose) { Debug.Log(response); }
			getDataStoreCallback (null);
		} 
		
		DataStore ds = (values[0] as DataStore);
		ds.setData(response.Substring(9));
		
		getDataStoreCallback (ds);
	}
	
	public delegate void RemoveDataStoreCallback (bool success);
	public RemoveDataStoreCallback removeDataStoreCallback;
	
	/**
	 * Remove a piece of data from Game Jolt's servers
	 * as specified by the type and key.
	 * 
	 * @param type The type of the Data Store. Should be either DataTypeStore.USER or DataTypeStore.GAME.
	 * @param key The key for which to remove the data.
	 */
	public void RemoveDataStore(DataStore.DataStoreType type, string key) {
		
		DataStore ds = new DataStore();
		ds.setKey(key);
		ds.setType(type);
		
		if (type == DataStore.DataStoreType.GAME) {
			Dictionary<string, string> paramss = new Dictionary<string, string>();
			paramss["key"] = (""+key+this.privateKey);
			string url = this.GetRequestURL("data-store/remove", paramss, false);
			paramss["key"] = key;
			paramss["signature"] = this.MD5(url);
			url = this.GetRequestURL("data-store/remove", paramss, false);
			
			StartCoroutine (OpenURLAndGetResponse(url, ReadRemoveGameDataStoreResponse, ds));

		}
		else {
			 this.Request("data-store/remove", "key=" + key, ReadRemoveGameDataStoreResponse, ds);
		}
	}
	
	/// <summary>
	/// Reads the remove game data store response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadRemoveGameDataStoreResponse (string response, params System.Object[] values) {
		if (verbose) { Debug.Log(response); }	
		
		if (response.Contains("success:\"false\"")
				|| response.Contains("message:\"There is no item with the key passed in.\"")) {
			if (verbose) { Debug.Log("GameJoltAPI: Could not get " + (values[0] as DataStore) + " DataStore with Key \"" + (values[0] as DataStore) + "\"."); }

			removeDataStoreCallback (false);
		}
		if (response.Contains("success:\"true\"")) {
			removeDataStoreCallback (true);
		}
	}
	
	public delegate void GetDataStoreKeysCallback (List<string> keys);
	public GetDataStoreKeysCallback getDataStoreKeysCallback;
	
	/**
	 * Retrieve a list of Data Store keys for the type specified by the parameter.
	 * 
	 * @param type The Type of keys to get, either DataStoreType.USER or DataStoreType.GAME.
	 */
	public void GetDataStoreKeys(DataStore.DataStoreType type) {
		if (type == DataStore.DataStoreType.GAME) {
			string urlstring = protocol + api_root + "v" + this.version + "/data-store/get-keys?game_id=" + this.gameId + this.privateKey;
			string signature = this.MD5(urlstring);
			Dictionary<string, string> paramss = new Dictionary<string, string>();
			paramss["signature"] = signature;
			string url = this.GetRequestURL("data-store/get-keys", paramss, false);
			
			StartCoroutine (OpenURLAndGetResponse(url, ReadGetDataStoreKeys, type));

		} else {
			if(!this.verified) {
				if (this.verbose) { Debug.Log("GameJoltAPI: Could not get the (currently verified) user."); }
				return;
			}
			this.Request("data-store/get-keys", "", ReadGetDataStoreKeys, type);
		}
	}
	
	/**
	 * Remove a piece of data from Game Jolt's servers
	 * as specified by the type and key.
	 * 
	 * @param type The type of the Data Store. Should be either DataTypeStore.USER or DataTypeStore.GAME.
	 * @param key The key for which to remove the data.
	 */
	public void ReadGetDataStoreKeys(string response, params System.Object[] values) {
		
		if ((DataStore.DataStoreType)values[0] == DataStore.DataStoreType.GAME) {

			string[] keys = response.Split('\n');
			if (!keys[0].Trim().Equals("success:\"true\"")) {
				if (verbose) { Debug.Log("GameJoltAPI: Could not get " + (DataStore.DataStoreType)values[0] + " DataStores."); }
				getDataStoreKeysCallback(null);
			}
			List<string> keys_list = new List<string>();
			for (int i = 1; i < keys.Length; i++) {
				if (keys[i].Equals("")) { continue; }
				
				int vsi = keys[i].IndexOf('"')+1;
				int vei = keys[i].LastIndexOf('"');
				keys_list.Add(keys[i].Substring(vsi, vei - vsi));
			}
		
			getDataStoreKeysCallback(keys_list);
		} else {
			if (verbose) { Debug.Log(response); } 
			string[] keys = response.Split('\n');
			if (!keys[0].Trim().Equals("success:\"true\"")) {
				if (verbose) {Debug.Log("GameJoltAPI: Could not get " + (DataStore.DataStoreType)values[0] + " DataStores."); }
				getDataStoreKeysCallback(null);
			}
			List<string> keys_list = new List<string>();
			for (int i = 1; i < keys.Length; i++) {
				if (keys[i].Equals("")) { continue; }
				
				int vsi = keys[i].IndexOf('"')+1;
				int vei = keys[i].LastIndexOf('"');
				
				keys_list.Add(keys[i].Substring(vsi, vei - vsi));
			}
			
			getDataStoreKeysCallback(keys_list);
		}
	}
	
	public delegate void SessionOpenCallback (bool success);
	public SessionOpenCallback sessionOpenCallback;
	
	/**
	 * Open a new play session with Game Jolt.
	 * This requires a user to be verified (logged in).
	 * You should do this before you ping or update. This will close the previously open session.
	 */
	public void SessionOpen() {
		if (!this.verified) { 
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not open Play Session: User must be verified.\n");
			}
			return; 
		}
		this.Request("sessions/open/", "", ReadSessionOpen, null);
	}
	
	/// <summary>
	/// Reads the session open.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>/
	public void ReadSessionOpen(string response, params System.Object[] values) {
		
		if (response.Contains("success:\"true\"")) {
			sessionOpenCallback(true);
		} else {
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not open Play Session.\n");
				Debug.Log(response);
			}
			sessionOpenCallback(false);
		}
	}
	
	public delegate void SessionUpdateCallback (bool success);
	public SessionUpdateCallback sessionUpdateCallback;
	
	/**
	 * Update the current play session with Game Jolt.
	 * You should call this every 60 seconds as Game Jolt closes play sessions after 120 seconds of inactivity.
	 * This requires a user to be verified (logged in).
	 * This method will return false if there is no session to update.
	 * 
	 * @param active You can set the game player as ACTIVE with true or IDLE with false. A good example of this would be sending an idle message if the user is on a menu or pause screen.
	 */
	public void SessionUpdate(bool active) {
		if (!this.verified) { 
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not update (ping) Play Session: User must be verified.\n");
			}
			return; 
		}
		this.Request("sessions/ping/", "", ReadSessionUpdate, null);
	}
	
	/// <summary>
	/// Reads the session update.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadSessionUpdate(string response, params System.Object[] values) {
		
		if (response.Contains("success:\"true\"")) {
			sessionUpdateCallback(true);
		} else {
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not update (ping) Play Session.\n");
				Debug.Log(response);
			}
			sessionUpdateCallback(false);
		}
	}
	
	public delegate void SessionCloseCallback (bool success);
	public SessionCloseCallback sessionCloseCallback;
	
	/**
	 * Close the current play session at Game Jolt.
	 * This requires a user to be verified (logged in).
	 * You should do this when closing or exiting your game.
	 * This method will return false if there is no session to close.
	 */
	public void SessionClose() {
		if (!this.verified) { 
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not close Play Session: User must be verified.\n");
			}
			return; 
		}
		this.Request("sessions/close/", "", ReadSessionUpdate, null);
	}
	
	/// <summary>
	/// Reads the session close.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadSessionClose(string response, params System.Object[] values) {
		
		if (response.Contains("success:\"true\"")) {
			sessionCloseCallback(true);
		} else {
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not close Play Session.\n");
				Debug.Log(response);
			}
			sessionCloseCallback(false);
		}
	}
	
	public delegate void AchieveTrophyCallback (bool success);
	public AchieveTrophyCallback achieveTrophyCallback;
	
	/**
	 * Give the currently verified user a trophy specified by Trophy object.
	 * This method uses the trophy's ID.
	 * @param t The Trophy to give.
	 */
	public void AchieveTrophy(Trophy t) {
		
		if (!this.verified) { 
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not Achieve Trophy: User must be verified.\n");
			}
			return; 
		}
		
		this.Request("trophies/add-achieved", "trophy_id=" + t.getId(), ReadAchieveTrophy, t.getId());
	}
	
	/**
	 * Give the currently verified user a trophy specified by Id.
	 * @param trophyId The ID of the Trophy to give.
	 */
	public void AchieveTrophy(int trophyId) {
		
		if (!this.verified) { 
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not Achieve Trophy: User must be verified.\n");
			}
			return; 
		}
		
		this.Request("trophies/add-achieved", "trophy_id=" + trophyId, ReadAchieveTrophy, trophyId);
	}
	
	/// <summary>
	/// Reads the achieve trophy.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadAchieveTrophy(string response, params System.Object[] values) {
		if (response.Contains("success:\"true\"")) {
			achieveTrophyCallback(true);
		} else {
			if (verbose) {
				Debug.Log("GameJoltAPI: Could not give Trophy to user.\n");
				Debug.Log(response);
			}
			achieveTrophyCallback(false);
		}
	}
	
	public delegate void GetTrophiesCallback (List<Trophy> trophies);
	public GetTrophiesCallback getTrophiesCallback;
	
	/**
	 * Get a list of all trophies.
	 */
	public void GetTrophies() {
		this.GetTrophies(Trophy.Achieved.EMPTY);
	}

	/**
	 * Get a list of trophies filtered with the Achieved parameter.
	 * The parameter can be Achieved.TRUE for achieved trophies, Achieved.FALSE for 
	 * unachieved trophies or Achieved.EMPTY for all trophies.
	 * @param a The type of trophies to get.
	 */
	public void GetTrophies(Trophy.Achieved a) {
		
		this.Request("trophies/", "achieved=" + a.ToString().ToLower(), ReadGetTrophies);
	}
	
	/// <summary>
	/// Reads the get trophies.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadGetTrophies(string response, params System.Object[] values) {
		
		List<Trophy> trophies = new List<Trophy>();
		string[] lines = response.Split('\n');
		Trophy t = null;
		for (int i = 1; i < lines.Length; i++) {
			if (lines[i].Equals("")) { continue; }
			
			int vsi = lines[i].IndexOf(':')+2;
			int vei  = lines[i].LastIndexOf('"');
			string key = lines[i].Substring(0, lines[i].IndexOf(':'));
			string value = lines[i].Substring(vsi , vei - vsi);
			if (key.Equals("id")) {
				t = new Trophy();
			}
			t.addProperty(key, value);
			if (key.Equals("achieved")) {
				trophies.Add(t);
			}
			
		}
		
		getTrophiesCallback(trophies);
	}
	
	public delegate void GetTrophyCallback (Trophy trophy);
	public GetTrophyCallback getTrophyCallback;
	
	/**
	 * Gets a single trophy from GameJolt as specified by trophyId
	 * @param trophyId The ID of the Trophy you want to get.
	 */
	public void GetTrophy(int trophyId) {
		this.Request("trophies/", "trophy_id=" + trophyId, ReadGetTrophy, trophyId);
	}
	
	/// <summary>
	/// Reads the get trophy.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadGetTrophy(string response, params System.Object[] values) {
		
		if (response.Contains("success:\"false\"")) {
		if (verbose) { Debug.Log("GameJoltAPI: Could not get Trophy with Id " + values[0] + "."); }
			getTrophyCallback(null);
		}
		if (response.Contains("REQUIRES_AUTHENTICATION")) {
			if (verbose) { Debug.Log("GameJoltAPI: User Authentication is required for this action."); }
			getTrophyCallback(null);
		}
		string[] lines = response.Split('\n');
		Trophy t = new Trophy();
		foreach (string line in lines) {
			if (line.Equals("")) { continue; }
			int vsi = line.IndexOf('"')+1;
			int vei = line.LastIndexOf('"'); 
			string key = line.Substring(0, line.IndexOf(":"));
			string value = line.Substring(vsi, vei - vsi);
			t.addProperty(key, value);
		}

		getTrophyCallback(t);
	}
	
	public delegate void GetRankCallback (int rank);
	public GetRankCallback getRankCallback;
	
	/**
	 * Retrieve a Rank of a submitted score
	 * 
	 * @param Score to get the rank fore
	 */
	public void GetRank(int score, string tableID) {
		
		Dictionary<string, string> param = new Dictionary<string, string>();
		param["table_id"] = tableID;
		param["sort"] = (""+score) + this.privateKey;

		string url = this.GetRequestURL("scores/get-rank", param, false);
		param["sort"] = (""+score);
		param["signature"] = this.MD5(url);
		if (verbose) { Debug.Log(url);}
		url = this.GetRequestURL("scores/get-rank", param, false);
		if (verbose) { Debug.Log(url);}
		
		StartCoroutine (OpenURLAndGetResponse(url, ReadGetRankResponse)); 
	}
	
	public void ReadGetRankResponse (string response, params System.Object[] values) {
			
		Debug.Log (response);
	}
	
	/**
	 * Retrieve all of the Highscores from GameJolt for the game in an array.
	 */
	public void GetHighscores() {
		this.GetHighscores(true, 100);
	}
	
	/**
	 * Retrieve a list of Highscores from GameJolt for either a game or the verified user.
	 * 
	 * @param all If set to true, this will retrieve all highscores. Otherwise it will retrieve the currently verified user's highscores.
	 */
	public void GetHighscores(bool all) {
		this.GetHighscores(all, 100);
	}
	
	public delegate void HighscoreCallback (List<Highscore> highscores);
	public HighscoreCallback highscoreCallback;
	
	/**
	 * Retrieve a list of Highscores from GameJolt for either a game or the verified user.
	 * 
	 * @param all If set to true, this will retrieve all highscores. Otherwise it will retrieve the currently verified user's highscores.
	 */
	public void GetHighscores(bool all, int limit) {
		
		if (all == false && !this.verified) { 
			if (verbose) { Debug.Log("GameJoltAPI: Could not get the Highscores for the verified user as the user is not verified."); }
			return;
		}
	
		Dictionary<string, string> paramsss = new Dictionary<string, string>();
		if (all == true) { // all highscores
			paramsss["limit"] = (""+limit) + this.privateKey;
			string url = this.GetRequestURL("scores", paramsss, false);
				
			paramsss["limit"] = ""+limit;
			paramsss["signature"] = this.MD5(url);
			url = this.GetRequestURL("scores", paramsss, false);
				
			StartCoroutine (OpenURLAndGetResponse(url, ReadHighscoreResponse)); 
		}				
		else { // verified user's highscores.
			paramsss["username"] = username;
			paramsss["user_token"] = usertoken+this.privateKey;  
			paramsss["limit"] = ""+limit;
			string url = this.GetRequestURL("scores", paramsss, true);
				
			paramsss["user_token"] = usertoken;  
			paramsss["signature"] = this.MD5(url);
			url = this.GetRequestURL("scores", paramsss, true);
				
			StartCoroutine (OpenURLAndGetResponse(url, ReadHighscoreResponse)); 
		}
	}
	
	/// <summary>
	/// Reads the highscore response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadHighscoreResponse (string response, params System.Object[] values) {
				
		if (verbose) { Debug.Log("GameJoltAPI: Highscore response recieved, Reading."); }
		
		List<Highscore> highscores = new List<Highscore>();
		
		string[] lines = response.Split('\n');
		if (!lines[0].Trim().Equals("success:\"true\"")) {
			if (verbose) { 
				Debug.Log("GameJoltAPI: Could not get the Highscores."); 
				Debug.Log(response);
			}
		}
		
		Highscore h = null;
		for (int i = 1; i < lines.Length; i++) {
			if (lines[i].Equals("")) { continue; }
				
			string key = lines[i].Substring(0, lines[i].IndexOf(':'));
			int vsi = lines[i].IndexOf(':')+2;
			int vei = lines[i].LastIndexOf('"');
			string value = lines[i].Substring( vsi, vei - vsi);
			if (key.Equals("score")) {
				h = new Highscore();
			}
			h.addProperty(key, value);
			if (key.Equals("stored")) {
				highscores.Add(h);
			}
		}
		
		highscoreCallback (highscores);
	}
	
	public delegate void AddHighscoreCallback (bool success);
	public AddHighscoreCallback addHighscoreCallback;
	
	/**
	 * Add a highscore for the currently verified Game Jolt user.
	 * @param score The string of the score, e.g. "5 Grapefruits". This is shown on the site.
	 * @param sort The sortable value of the score, e.g. 5. This is shown on the site.
	 */
	public void AddHighscore(string score, int sort) {
		AddHighscore(score, sort, "");
	}
	
	/**
	 * Add a highscore for the currently verified Game Jolt user.
	 * @param score The string of the score, e.g. "5 Grapefruits". This is shown on the site.
	 * @param sort The sortable value of the score, e.g. 5. This is shown on the site.
	 * @param extra Extra information to be stored about this score as a string, such as how the score was made, or time taken. This is not shown on the site.
	 */
	public void AddHighscore(string score, int sort, string extra) {
		if (!this.verified) {
			if (verbose) { Debug.Log("GameJoltAPI: Could not add the High Score because the user is not verified."); }
			return;
		}

		Dictionary<string, string> paramsss = new Dictionary<string, string>();
		paramsss["username"] = username;
		paramsss["user_token"] = usertoken+this.privateKey;  
		paramsss["score"] = ""+score;
		paramsss["extra_data"] = ""+extra;
		paramsss["sort"] = ""+sort;
		
		string url = this.GetRequestURL("scores/add", paramsss, true);
		
		paramsss["user_token"] = usertoken;
		paramsss["signature"] = this.MD5(url);
		
		url = this.GetRequestURL("scores/add", paramsss, true);
		if (verbose) { Debug.Log(url);}
		
		StartCoroutine (OpenURLAndGetResponse(url, ReadAddHighscoreResponse)); 
	}
	
	/// <summary>
	/// Reads the add highscore response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadAddHighscoreResponse (string response, params System.Object[] values) {
			
		bool success = true;
		
		if (response.Contains("success:\"false\"") || response.Equals("REQUEST_FAILED")) {
			if (verbose) { Debug.Log("GameJoltAPI: Could not add the High Score."); }
			if (verbose) { Debug.Log(response); }
			success = false;
		}
		
		addHighscoreCallback(success);
	}
	
	/**
	 * Adds a HighScore for a Guest!
	 * @param guest_username The desired name of the guest you want to add the highscore for.
	 * @param score The string of the score, e.g. "5 Grapefruits". This is shown on the site.
	 * @param sort The sortable value of the score, e.g. 5. This is shown on the site.
	 */
	public void AddHighscore(string guest_username, string score, int sort) {
		AddHighscore(guest_username, score, sort, "");
	}
	
	/**
	 * Adds a HighScore for a Guest with additional data!
	 * @param guest_username The desired name of the guest you want to add the highscore for.
	 * @param score The string of the score, e.g. "5 Grapefruits". This is shown on the site.
	 * @param sort The sortable value of the score, e.g. 5. This is shown on the site.
	 * @param extra Extra information to be stored about this score as a string, such as how the score was made, or time taken. This is not shown on the site.
	 */
	public void AddHighscore(string guest_username, string score, int sort, string extra) {
		Dictionary<string, string> paramsss = new Dictionary<string, string>();
		paramsss["guest"] = guest_username;
		paramsss["score"] = ""+score;
		paramsss["sort"] = ""+sort;
		paramsss["extra_data"] = (""+extra) + this.privateKey;
		
		string url = this.GetRequestURL("scores/add", paramsss, false);
		
		paramsss["extra_data"] = ""+extra;
		paramsss["signature"] = this.MD5(url);
		
		url = this.GetRequestURL("scores/add", paramsss, false);
		
		StartCoroutine (OpenURLAndGetResponse(url, ReadAddGuestHighscoreResponse)); 
	}
	
	/// <summary>
	/// Reads the add guest highscore response.
	/// </summary>
	/// <param name='response'>
	/// Response.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	public void ReadAddGuestHighscoreResponse (string response, params System.Object[] values) {
			
		bool success = true;
		
		if (verbose) { Debug.Log(response); }
		if (response.Contains("success:\"false\"") || response.Equals("REQUEST_FAILED")) {
			if (verbose) { Debug.Log("GameJoltAPI: Could not add the Guest High Score."); }
			if (verbose && response.Contains("Guests are not allowed to enter scores for this game.")) { // TODO: optimisation.
				Debug.Log("Guests are not allowed to enter scores for this game.");
			}
			if (verbose) { Debug.Log(response); }
			success = false;
		}
		
		addHighscoreCallback(success);
	}

	/**
	 * Calculates an MD5 hash.
	 * @param s The string you want the hash of.
	 */
	private string MD5(string input)
	{
		System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] data = System.Text.Encoding.ASCII.GetBytes(input);
        data = x.ComputeHash(data);
        string ret = "";
        for (int i=0; i < data.Length; i++)
                ret += data[i].ToString("x2").ToLower();
        return ret;
	}
	
	/**
	 * Make a request to the GameJolt API.
	 * @param method The GameJolt API method, such as "game-api/add-trophy", without the "game-api/" part.
	 * @param paramss A map of the parameters you want to include. 
	 * 				 Note that if the user is verified you do not have to include the username/user_token/game_id.
	 */
	public void Request(string method, Dictionary<string, string> paramss, Action<string, System.Object[]> responseCallback, params System.Object[] values) {
		this.Request(method, paramss, true, responseCallback, values);
	}
	
	/**
	 * Perform a GameJolt API request.
	 * Use this one if you know your HTTP requests.
	 * @param method The API method to call. Note that gamejolt.com/api/game/ is already prepended.
	 * @param paramssLine The GET request paramss, such as "trophy_id=23&achieved=empty".
	 */
	public void Request(string method, string paramssLine, Action<string, System.Object[]> responseCallback, params System.Object[] values) 
	{
		this.Request(method, paramssLine, true, responseCallback, values);
	}
	
	/// <summary>
	/// Request the specified method, paramssLine, requireVerified, responseCallback and values.
	/// </summary>
	/// <param name='method'>
	/// Method.
	/// </param>
	/// <param name='paramssLine'>
	/// Paramss line.
	/// </param>
	/// <param name='requireVerified'>
	/// Require verified.
	/// </param>
	/// <param name='responseCallback'>
	/// Response callback.
	/// </param>
	/// <param name='values'>
	/// Values.
	/// </param>
	private void Request(string method, string paramssLine, bool requireVerified , Action<string, System.Object[]> responseCallback, params System.Object[] values) {
		Dictionary<string, string> ps = new Dictionary<string, string>();
		string[] paramss = paramssLine.Split('&');
		for (int i = 0; i < paramss.Length; i++) {
			if (paramss[i].Length == 0) {
				continue;
			}
			
			string[] s = paramss[i].Split('=');
			
			string key = s[0];
			string value = (s.Length==1)?"":s[1];
			ps[key] = value;
		}
		this.Request(method, ps, requireVerified, responseCallback, values);
	}
	
	/**
	 * Make a request to the GameJolt API.
	 * @param method The GameJolt API method, such as "add-trophy", without the "game-api/" part.
	 * @param paramss A map of the parameters you want to include. 
	 * 				 Note that if the user is verified you do not have to include the username/user_token/game_id.
	 * @param requireVerified This is only set to false when checking if the user is verified.
	 */
	private void Request(string method, Dictionary<string, string> paramss, bool requireVerified, Action<string, System.Object[]> responseCallback, params System.Object[] values)
	{
		if (requireVerified && !this.verified) {
			responseCallback("REQUIRES_AUTHENTICATION", null);
			return;
		}
			
		if (!this.verified) {
			string user_token = paramss["user_token"];
			paramss["user_token"] = paramss["user_token"] + privateKey;
			string urlstring = this.GetRequestURL(method, paramss);
			string signature = this.MD5(urlstring);
				
			paramss["user_token"] = user_token;
			paramss["signature"] = signature;
		} else {
			// string user_token = paramss.get("user_token");
			paramss["user_token"] = this.usertoken + privateKey;
			paramss["username"] = this.username;
			string urlstring = this.GetRequestURL(method, paramss);
			string signature = this.MD5(urlstring);
				
			paramss["user_token"] = this.usertoken;			
			paramss["signature"] = signature;
		}
			
		string urlstr = this.GetRequestURL(method, paramss);
		StartCoroutine(OpenURLAndGetResponse(urlstr, responseCallback, values));
	}
	
	/**
	 * Performs the HTTP Request.
	 * @param urlstring The URL to HTTP Request.
	 * @return The HTTP Response.
	 */
	IEnumerator OpenURLAndGetResponse(string urlstring, Action<string, System.Object[]> responseCallback, params System.Object[] values)
	{
		if(verbose) { Debug.Log ("Opening URL : " + urlstring); }
		
		WWW www = new WWW(urlstring);
	
		yield return www;
		
		responseCallback(www.text, values);
	}
	
	/**
	 * Get the full request url from the parameters given.
	 * @param method The GameJolt API method, such as "game-api/add-trophy".
	 * @param paramss A map of the parameters you want to include. 
	 * @return The full request url.
	 * @throws UnsupportedEncodingException 
	 */
	private string GetRequestURL(string method, Dictionary<string, string> paramss) {
		return this.GetRequestURL(method, paramss, true);
	}
	
	/**
	 * Get the full request url from the parameters given.
	 * @param method The GameJolt API method, such as "game-api/add-trophy".
	 * @param paramss A map of the parameters you want to include. 
	 * @param addUserToken Whether or not to add the UserToken to the url.
	 * @return The full request url.
	 * @throws UnsupportedEncodingException 
	 */
	private string GetRequestURL(string method, Dictionary<string, string> paramss, bool addUserToken) {
		string urlstring = protocol + api_root + "v" + this.version + "/" + method + "?game_id=" + this.gameId;

		string user_token = "";
		foreach(KeyValuePair<string,string> entry in paramss)
		{
			// do something with entry.Value or entry.Key
			string k = entry.Key;
			string v = entry.Value;
			if (k.Equals("user_token")) {
				user_token = v;
				continue;
			}
			urlstring += "&" + k + "=" + WWW.EscapeURL(v);
		}
		if (addUserToken) {
			urlstring += "&user_token=" + user_token;
		}
		return urlstring;
	}
	
}