using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

/**
 * <b>DataStore</b><br/>
 * This class is only instantiated from within the org.gamejolt.* package.
 * 
 * @author Ashley Gwinnell
 * @since 0.95
 * @version 0.95
 */
public class DataStore  {

	/** This enum is used in the GameJoltAPI class to determine which datastores to retrieve */
	public enum DataStoreType {USER, GAME};
	
	private string key;
	private string data;
	private DataStoreType type;
	
	/**
	 * Creates a new DataStore
	 */
	public DataStore() {
		
	}
	public void setKey(string key) {
		this.key = key;
	}
	public void setData(string data) {
		this.data = data;
	}
	public void setType(DataStoreType type) {
		this.type = type;
	}
	
	/**
	 * Retrieve the unique identifying 'key' of this Data Store.
	 * @return the unique identifying 'key' of this Data Store.
	 */
	public string getKey() {
		return this.key;
	}
	
	/**
	 * Retrieve the data String of this Data Store.
	 * @return the data String of this Data Store.
	 */
	public string getData() {
		return this.data;
	}
	
	/**
	 * Retrieve the type of this Data Store. Does it belong to the user or the game.
	 * @return the type of this Data Store.
	 */
	public DataStoreType getType() {
		return this.type;
	}
	
	public override string ToString() {
		StringBuilder b = new StringBuilder(255);
		b.Append("DataStore [key=");
		b.Append(this.key);
		b.Append(", data=");
		b.Append(this.data);
		b.Append(", type=");
		b.Append(this.type);
		b.Append("]");
		return b.ToString();
	}
	
}